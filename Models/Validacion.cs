﻿using PrimerAvanze.Models;
using System;
using System.ComponentModel.DataAnnotations;
using System.Linq;

namespace PrimerAvanze.Models
{
    public class Validacion
    {

        [AttributeUsage(AttributeTargets.Property | AttributeTargets.Field | AttributeTargets.Parameter, AllowMultiple = false)]
        internal sealed class RangofechaAttribute : ValidationAttribute
        {
            private readonly ApplicationDbContext db = new ApplicationDbContext();
            private string pruebaId;
            private DateTime fechaini;
            private DateTime fechafin;
            private double valorMinimo = 0;
            private double valorMaximo = 0;

            public RangofechaAttribute(string atributoProducto)
            {

                this.pruebaId = atributoProducto;
            }

            protected override ValidationResult IsValid(object value, ValidationContext validationContext)
            {
                //Aquí tenemos que usar otra función para pasar el contexto de validación y usar reflection

                int? contenedorpruebaId = obtenerProductoId(validationContext);

                if (contenedorpruebaId == null) return ValidationResult.Success;



                Prueba topes2 = db.Pruebas.Where(x => x.ID == contenedorpruebaId ).SingleOrDefault();
                this.fechaini = topes2.FechaInicio;
                this.fechafin = topes2.FechaFinal;
                var fechaenviada = Convert.ToDateTime(value);

                if (topes2 == null)
                {
                    this.ErrorMessage = "NO se encontro el dato en la base de datos";
                    ValidationResult validacionError = new ValidationResult(this.ErrorMessageString, new string[] { validationContext.MemberName });
                    return validacionError;
                }

                if (fechaenviada <= topes2.FechaFinal && fechaenviada <= topes2.FechaFinal)
                {

                    return ValidationResult.Success;
                }
                else
                {
                    this.ErrorMessage = FormatErrorMessage(validationContext.DisplayName);
                    ValidationResult validacionError = new ValidationResult(this.ErrorMessageString, new string[] { validationContext.MemberName });

                    return validacionError;

                }

                //Pregunta topes2 = db.Preguntas.Where(x => x.ID == contenedorpruebaId && x.estado == estado.ACTIVO).SingleOrDefault();

                //IEnumerable<ProductoValor> topes = db.ProductoValores.Where(x => x.ProductoId == productoId && x.FechaRegistro == db.ProductoValores.Max(y => y.FechaRegistro)).toList();

                //ProductoValor topes0 = db.ProductoValores.Where(x => x.ProductoId == productoId && x.ValorMaximo > 0).OrderByDescending(x => x.FechaRegistro).FirstOrDefault();
                //ProductoValor topes1 = db.ProductoValores.Where(x => x.ProductoId == productoId && x.FechaRegistro == db.ProductoValores.Max(y => y.FechaRegistro)).SingleOrDefault();
                //ProductoValor topes2 = db.ProductoValores.Where(x => x.ProductoId == productoId).Last();
                //ProductoValor topes3 = db.ProductoValores.Where(x => x.ProductoId == productoId).OrderByDescending(x => x.FechaRegistro).FirstOrDefault();
                //ProductoValor topes4 = db.ProductoValores.Where(x => x.ProductoId == productoId).LastOrDefault();

                //if (topes0 == null) return ValidationResult.Success;
                //if (topes0.ID <= 0) return ValidationResult.Success;

                //var valor = Convert.ToDouble(value);

                //if (valor >= topes0.ValorMinimo && valor <= topes0.ValorMaximo) return ValidationResult.Success;

                //this.valorMinimo = topes0.ValorMinimo;
                //this.valorMaximo = topes0.ValorMaximo;
                //this.ErrorMessage = FormatErrorMessage(validationContext.DisplayName);
                //ValidationResult validacionError = new ValidationResult(this.ErrorMessageString, new string[] { validationContext.MemberName });

                //return validacionError;
                return null;
            }

            public override string FormatErrorMessage(string name)
            {
                return string.Format("El campo {0} debe estar entre {1} y {2}", name, this.fechaini, this.fechafin);
            }

            private int? obtenerProductoId(ValidationContext validationContext)
            {
                var propertyInfo = validationContext.ObjectType.GetProperty(this.pruebaId);
                if (propertyInfo != null)
                {
                    var resultado = propertyInfo.GetValue(validationContext.ObjectInstance, null);
                    return resultado as int?;
                }
                return -1;
            }
        }
    }
}
