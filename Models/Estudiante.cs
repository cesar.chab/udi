﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace PrimerAvanze.Models
{
    public class Estudiante
    {
        public int ID { get; set; }

        public string Nombre { get; set; }

        public string Apellido { get; set; }

        public string Email { get; set; }

        [Display(Name="Programa de pregrado ")]
        [ForeignKey("ProgramaAcademico")]

        public int? ProgramaAcademicoID { get; set; }

        public ProgramaAcademico ProgramaAcademico { get; set; }

        [ForeignKey("User")]
        public string UsuarioID { get; set; }
        public virtual ApplicationUser User { get; set; }


    }
}