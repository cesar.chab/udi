﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
namespace PrimerAvanze.Models
{
    
   

        [AttributeUsage(AttributeTargets.Property | AttributeTargets.Field
            | AttributeTargets.Parameter, AllowMultiple = false)]

        internal sealed class BirthDateRangeAttribute : ValidationAttribute
        {
            public DateTime MinimumMar = new DateTime(2020, 03, 01);
            public DateTime MaximumMar = new DateTime(2020, 03, 31);
            public DateTime MinimumSep = new DateTime(2020, 09, 01);
            public DateTime MaximumSep = new DateTime(2020, 09, 30);
            public BirthDateRangeAttribute() { }

            public override bool IsValid(object value)
            {
                var minMar = (IComparable)MinimumMar;
                var maxMar = (IComparable)MaximumMar;
                var minSep = (IComparable)MinimumSep;
                var maxSep = (IComparable)MaximumSep;
                return ((minMar.CompareTo(value) <= 0) && (maxMar.CompareTo(value) >= 0)) || ((minSep.CompareTo(value) <= 0) && (maxSep.CompareTo(value) >= 0));
            }

            public override string FormatErrorMessage(string name)
            {

                var msg = string.Format("La fecha seleccionada no está disponible, por favor seleccione una fecha del mes de Marzo" +
                    " o Septiembre que es la fecha habilitada de inscripcion.");
                return msg;
            }
        }
    }


