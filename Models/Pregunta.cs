﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace PrimerAvanze.Models
{
    public class Pregunta
    {
        public int ID { get; set; }

        [StringLength(70, ErrorMessage ="Maximo 70 caracteres")]
        [Display(Name ="Pregunta")]
        public string Descripcion { get; set; }
        public int puntaje { get; set; }
        public Estado estado { get; set; }

        [Required]
        [Display(Name = "Opcion A")]
        public string OpcionA { get; set; }

        [Required]
        [Display(Name = "Opcion B")]
        public string OpcionB { get; set; }

        [Required]
        [Display(Name = "Opcion C")]
        public string OpcionC { get; set; }

        [Required]
        [Display(Name = "Opcion D")]
        public string OpcionD { get; set; }

        [ForeignKey("Competencia")]
        
        [Display(Name ="Competencia")]
       public int? id_competencia { get; set; }

        public Competencia Competencia { get; set; }


    }
}