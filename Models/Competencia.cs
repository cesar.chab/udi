﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PrimerAvanze.Models
{
    public class Competencia
    {
        public int ID { get; set; }
        public string Nombre { get; set; }

        public Estado estado { get; set; }
    }
}