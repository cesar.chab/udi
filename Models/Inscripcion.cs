﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Microsoft.Ajax.Utilities;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Dynamic;
using static PrimerAvanze.Models.Validacion;

namespace PrimerAvanze.Models
{
    public class Inscripcion
    {
        public int ID { get; set; }

        public string Nombre { get; set; }

        public string Apellido { get; set; }

        public int Identificacion { get; set; }

        public string Email { get; set; }

        public int semestre { get; set; }

        public string Programa { get; set; }


        [ForeignKey("Prueba")]
        public int? PruebaID { get; set; }


        public Prueba Prueba { get; set; }

        [Rangofecha("PruebaID")]

       

        //[Required]
       // [BirthDateRange]
       [DataType(DataType.Date)]
       [Display(Name = "Fecha de Inscripcion")]
       [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
        public DateTime Fecha { get; set; }

    }
}