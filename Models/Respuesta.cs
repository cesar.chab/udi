﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace PrimerAvanze.Models
{
    public class Respuesta
    {

        public int ID { get; set; }

        [Display(Name ="Respuesta")]
        public Resul Resul { get; set; }
        public string Opcion { get; set; }

        [Display(Name = "Pregunta")]
        public int PreguntaID { get; set; }
        public Pregunta Pregunta { get; set; }

    }
}