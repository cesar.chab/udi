﻿using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using System.Collections.Generic;
using System.Data.Entity;
using System.Security.Claims;
using System.Threading.Tasks;

namespace PrimerAvanze.Models
{
    // You can add profile data for the user by adding more properties to your ApplicationUser class, please visit http://go.microsoft.com/fwlink/?LinkID=317594 to learn more.
    public class ApplicationUser : IdentityUser
    {
        public async Task<ClaimsIdentity> GenerateUserIdentityAsync(UserManager<ApplicationUser> manager)
        {
            // Note the authenticationType must match the one defined in CookieAuthenticationOptions.AuthenticationType
            var userIdentity = await manager.CreateIdentityAsync(this, DefaultAuthenticationTypes.ApplicationCookie);
            // Add custom user claims here
            return userIdentity;
        }

        public virtual ICollection<Estudiante> Estudiantes { get; set; }



    }

    public class ApplicationDbContext : IdentityDbContext<ApplicationUser>
    {
        public ApplicationDbContext()
            : base("DefaultConnection", throwIfV1Schema: false)
        {
        }

        static ApplicationDbContext()
        {
            // Set the database intializer which is run once during application start
            // This seeds the database with admin user credentials and admin role
            Database.SetInitializer<ApplicationDbContext>(new ApplicationDbInitializer());
        }

        public static ApplicationDbContext Create()
        {
            return new ApplicationDbContext();
        }

        public DbSet<PrimerAvanze.Models.Inscripcion> Inscripcions { get; set; }
        public DbSet<PrimerAvanze.Models.Pregunta> Preguntas { get; set; }
        public DbSet<PrimerAvanze.Models.Respuesta> Respuestas { get; set; }
        public DbSet<PrimerAvanze.Models.Prueba> Pruebas { get; set; }
        public DbSet<PrimerAvanze.Models.Estudiante> Estudiantes { get; set; }
        public DbSet<PrimerAvanze.Models.ProgramaAcademico> ProgramaAcademicos { get; set; }
        public DbSet<PrimerAvanze.Models.Competencia> Competencias { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            modelBuilder.Entity<Estudiante>()
                .HasRequired(c => c.User)
                .WithMany(t => t.Estudiantes)
                .Map(n => n.MapKey("UserID"));

        }

    }
}