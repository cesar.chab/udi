namespace PrimerAvanze.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class collec : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.Estudiantes", "UserID", "dbo.AspNetUsers");
            DropIndex("dbo.Estudiantes", new[] { "UserID" });
            AddColumn("dbo.Estudiantes", "UsuarioID", c => c.String());
            AlterColumn("dbo.Estudiantes", "UserID", c => c.String(nullable: false, maxLength: 128));
            CreateIndex("dbo.Estudiantes", "UserID");
            AddForeignKey("dbo.Estudiantes", "UserID", "dbo.AspNetUsers", "Id", cascadeDelete: true);
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Estudiantes", "UserID", "dbo.AspNetUsers");
            DropIndex("dbo.Estudiantes", new[] { "UserID" });
            AlterColumn("dbo.Estudiantes", "UserID", c => c.String(maxLength: 128));
            DropColumn("dbo.Estudiantes", "UsuarioID");
            CreateIndex("dbo.Estudiantes", "UserID");
            AddForeignKey("dbo.Estudiantes", "UserID", "dbo.AspNetUsers", "Id");
        }
    }
}
