namespace PrimerAvanze.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class camposprueba : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.Pruebas", "Competencia_ID", "dbo.Competencias");
            DropIndex("dbo.Pruebas", new[] { "Competencia_ID" });
            RenameColumn(table: "dbo.Pruebas", name: "Competencia_ID", newName: "CompetenciaID");
            AddColumn("dbo.Pruebas", "Nombre", c => c.String());
            AddColumn("dbo.Pruebas", "estado", c => c.Int(nullable: false));
            AlterColumn("dbo.Pruebas", "CompetenciaID", c => c.Int(nullable: false));
            CreateIndex("dbo.Pruebas", "CompetenciaID");
            AddForeignKey("dbo.Pruebas", "CompetenciaID", "dbo.Competencias", "ID", cascadeDelete: true);
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Pruebas", "CompetenciaID", "dbo.Competencias");
            DropIndex("dbo.Pruebas", new[] { "CompetenciaID" });
            AlterColumn("dbo.Pruebas", "CompetenciaID", c => c.Int());
            DropColumn("dbo.Pruebas", "estado");
            DropColumn("dbo.Pruebas", "Nombre");
            RenameColumn(table: "dbo.Pruebas", name: "CompetenciaID", newName: "Competencia_ID");
            CreateIndex("dbo.Pruebas", "Competencia_ID");
            AddForeignKey("dbo.Pruebas", "Competencia_ID", "dbo.Competencias", "ID");
        }
    }
}
