// <auto-generated />
namespace PrimerAvanze.Migrations
{
    using System.CodeDom.Compiler;
    using System.Data.Entity.Migrations;
    using System.Data.Entity.Migrations.Infrastructure;
    using System.Resources;
    
    [GeneratedCode("EntityFramework.Migrations", "6.1.3-40302")]
    public sealed partial class camposprueba : IMigrationMetadata
    {
        private readonly ResourceManager Resources = new ResourceManager(typeof(camposprueba));
        
        string IMigrationMetadata.Id
        {
            get { return "202010262226174_camposprueba"; }
        }
        
        string IMigrationMetadata.Source
        {
            get { return null; }
        }
        
        string IMigrationMetadata.Target
        {
            get { return Resources.GetString("Target"); }
        }
    }
}
