namespace PrimerAvanze.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class agregarval : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Inscripcions", "PruebaID", c => c.Int());
            CreateIndex("dbo.Inscripcions", "PruebaID");
            AddForeignKey("dbo.Inscripcions", "PruebaID", "dbo.Pruebas", "ID");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Inscripcions", "PruebaID", "dbo.Pruebas");
            DropIndex("dbo.Inscripcions", new[] { "PruebaID" });
            DropColumn("dbo.Inscripcions", "PruebaID");
        }
    }
}
