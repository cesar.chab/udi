namespace PrimerAvanze.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class prueba1 : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Pruebas",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        FechaInicio = c.DateTime(nullable: false),
                        FechaFinal = c.DateTime(nullable: false),
                        Competencia_ID = c.Int(),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.Competencias", t => t.Competencia_ID)
                .Index(t => t.Competencia_ID);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Pruebas", "Competencia_ID", "dbo.Competencias");
            DropIndex("dbo.Pruebas", new[] { "Competencia_ID" });
            DropTable("dbo.Pruebas");
        }
    }
}
