namespace PrimerAvanze.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class tablaestuProgra : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Estudiantes",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        Nombre = c.String(),
                        Apellido = c.String(),
                        Email = c.String(),
                        ProgramaAcademicoID = c.Int(),
                        UserID = c.String(maxLength: 128),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.ProgramaAcademicoes", t => t.ProgramaAcademicoID)
                .ForeignKey("dbo.AspNetUsers", t => t.UserID)
                .Index(t => t.ProgramaAcademicoID)
                .Index(t => t.UserID);
            
            CreateTable(
                "dbo.ProgramaAcademicoes",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        Nombre = c.String(),
                    })
                .PrimaryKey(t => t.ID);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Estudiantes", "UserID", "dbo.AspNetUsers");
            DropForeignKey("dbo.Estudiantes", "ProgramaAcademicoID", "dbo.ProgramaAcademicoes");
            DropIndex("dbo.Estudiantes", new[] { "UserID" });
            DropIndex("dbo.Estudiantes", new[] { "ProgramaAcademicoID" });
            DropTable("dbo.ProgramaAcademicoes");
            DropTable("dbo.Estudiantes");
        }
    }
}
