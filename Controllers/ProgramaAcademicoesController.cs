﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using PrimerAvanze.Models;

namespace PrimerAvanze.Controllers
{
    public class ProgramaAcademicoesController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        // GET: ProgramaAcademicoes
        public ActionResult Index()
        {
            return View(db.ProgramaAcademicos.ToList());
        }

        // GET: ProgramaAcademicoes/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ProgramaAcademico programaAcademico = db.ProgramaAcademicos.Find(id);
            if (programaAcademico == null)
            {
                return HttpNotFound();
            }
            return View(programaAcademico);
        }

        // GET: ProgramaAcademicoes/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: ProgramaAcademicoes/Create
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que quiere enlazarse. Para obtener 
        // más detalles, vea https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "ID,Nombre")] ProgramaAcademico programaAcademico)
        {
            if (ModelState.IsValid)
            {
                db.ProgramaAcademicos.Add(programaAcademico);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(programaAcademico);
        }

        // GET: ProgramaAcademicoes/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ProgramaAcademico programaAcademico = db.ProgramaAcademicos.Find(id);
            if (programaAcademico == null)
            {
                return HttpNotFound();
            }
            return View(programaAcademico);
        }

        // POST: ProgramaAcademicoes/Edit/5
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que quiere enlazarse. Para obtener 
        // más detalles, vea https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "ID,Nombre")] ProgramaAcademico programaAcademico)
        {
            if (ModelState.IsValid)
            {
                db.Entry(programaAcademico).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(programaAcademico);
        }

        // GET: ProgramaAcademicoes/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ProgramaAcademico programaAcademico = db.ProgramaAcademicos.Find(id);
            if (programaAcademico == null)
            {
                return HttpNotFound();
            }
            return View(programaAcademico);
        }

        // POST: ProgramaAcademicoes/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            ProgramaAcademico programaAcademico = db.ProgramaAcademicos.Find(id);
            db.ProgramaAcademicos.Remove(programaAcademico);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
